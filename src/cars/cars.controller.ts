import { Body, Controller, Delete, Get, Param, Patch, Post } from '@nestjs/common';
import { CarsService } from './cars.service';
import { CreateCarDto } from './dto/create-cars.dto';
import { UpdateCarDto } from './dto/update-cars.dto';


@Controller('cars')
export class CarsController {

    constructor(private carsService: CarsService){}

    @Post()
    create(@Body() carDto: CreateCarDto){
        return this.carsService.create(carDto)
    }


    @Get()
    getAll(){
        return this.carsService.getAllCar()
    }



    @Patch(':id')
    update(@Param('id') id: number, @Body() updateCarDto: UpdateCarDto) {
        return this.carsService.update(id, updateCarDto);
    }

    @Delete(':id')
    remove(@Param('id') id: number) {
        return this.carsService.remove(id);
  }
    
}
