import { Column, DataType, Model, Table } from "sequelize-typescript";


interface CarCreationAttribute{
    id: number,
    year: number,
    name: string,
    price: number,
    

}

@Table({tableName: 'cars'})
export class Car extends Model<Car, CarCreationAttribute>{

    @Column({type: DataType.INTEGER, unique: true, autoIncrement: true, primaryKey: true})
    id: number;

    @Column({type: DataType.INTEGER,allowNull: false})
    year: number;

    @Column({type: DataType.STRING, allowNull: false})
    name: string;

    @Column({type: DataType.INTEGER, allowNull: false})
    price: number;

    
    

}