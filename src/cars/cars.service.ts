import { HttpException, HttpStatus, Injectable } from '@nestjs/common';
import { InjectModel } from '@nestjs/sequelize';
import { Car } from './cars.model';
import { CreateCarDto } from './dto/create-cars.dto';
import { UpdateCarDto } from './dto/update-cars.dto';


@Injectable()
export class CarsService {

    constructor(@InjectModel(Car) private carRepository: typeof Car){}

    async create(dto: CreateCarDto){
        const cars = await this.carRepository.create(dto)
        return cars
    }
    async getAllCar(){
        const cars = await this.carRepository.findAll({include:{all: true}})
        return cars

    }

    async update(id: number, dto: UpdateCarDto){
        const car =  await this.carRepository.findByPk(id);
        if (!car) {
          throw new HttpException(`Car with ID ${id} not found`,HttpStatus.NOT_FOUND);
        }
        return await this.carRepository.update(dto,{where: {id}})
      }
    
    async remove(id: number){
        const car =  await this.carRepository.findByPk(id);
        if (!car) {
            throw new HttpException(`Car with ID ${id} not found`,HttpStatus.NOT_FOUND);
          }
        return this.carRepository.destroy({where:{id}})
    }

    

    async getAllCars() {
      const cars = await this.carRepository.findAll();
      return cars;

    }
}
