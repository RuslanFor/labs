export class CreateCarDto{
    readonly year: number;
    readonly name: string;
    readonly price: number;
}