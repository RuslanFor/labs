export class UpdateCarDto{
    readonly year: number;
    readonly name: string;
    readonly price: number;
}